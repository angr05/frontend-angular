export const environment = {
  production: false,
  hmr: true,
  hasStoreDevTools: true
};
