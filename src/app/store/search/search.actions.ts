import {
  MostPopularTech,
  RecentlyUpdatedTech,
  SearchResult,
  SearchResults,
  SearchResultsCollection,
  TopCategories
} from '../../library/search/models/search-results.model';
import { ofType, unionize, UnionOf } from 'unionize';

import { Action } from '@ngrx/store';
import { SearchState } from './search.state';

export enum SearchResultTypes {
  GET_SEARCH_RESULTS = '[Search] GET_SEARCH_RESULTS',
  GET_SEARCH_RESULTS_NEXTPAGE = '[Search] GET_SEARCH_RESULTS_NEXTPAGE',
  GET_SEARCH_RESULTS_NEXTPAGE_SUCCESS = '[Search] GET_SEARCH_RESULTS_NEXTPAGE_SUCCESS',
  GET_SEARCH_RESULTS_SUCCESS = '[Search] GET_SEARCH_RESULTS_SUCCESS',
  GET_SEARCH_RESULTS_ERROR = '[Search] GET_SEARCH_RESULTS_ERROR',
  GET_TOP_CATEGORIES = '[Silo] GET_TOP_CATEGORIES',
  GET_TOP_CATEGORIES_SUCCESS = '[Silo] GET_TOP_CATEGORIES_SUCCESS',
  GET_TOP_CATEGORIES_ERROR = '[Silo] GET_TOP_CATEGORIES_ERROR',

  GET_FEATURED_TOOLS_AND_SERVICE = '[Silo] GET_FEATURED_TOOLS_AND_SERVICE',
  GET_FEATURED_TOOLS_AND_SERVICE_SUCCESS = '[Silo] GET_FEATURED_TOOLS_AND_SERVICE_SUCCESS',
  GET_FEATURED_TOOLS_AND_SERVICE_ERROR = '[Silo] GET_FEATURED_TOOLS_AND_SERVICE_ERROR',

  GET_FEATURED_LANGUAGE_RESOURCES = '[Silo] GET_FEATURED_LANGUAGE_RESOURCES',
  GET_FEATURED_LANGUAGE_RESOURCES_SUCCESS = '[Silo] GET_FEATURED_LANGUAGE_RESOURCES_SUCCESS',
  GET_FEATURED_LANGUAGE_RESOURCES_ERROR = '[Silo] GET_FEATURED_LANGUAGE_RESOURCES_ERROR',

  GET_FEATURED_ORGANIZATIONS = '[Silo] GET_FEATURED_ORGANIZATIONS',
  GET_FEATURED_ORGANIZATIONS_SUCCESS = '[Silo] GET_FEATURED_ORGANIZATIONS_SUCCESS',
  GET_FEATURED_ORGANIZATIONS_ERROR = '[Silo] GET_FEATURED_ORGANIZATIONS_ERROR',

  GET_RECENTLY_UPDATED = '[Silo] GET_RECENTLY_UPDATED',
  GET_RECENTLY_UPDATED_SUCCESS = '[Silo] GET_RECENTLY_UPDATED_SUCCESS',
  GET_RECENTLY_UPDATED_ERROR = '[Silo] GET_RECENTLY_UPDATED_ERROR',

  GET_MOST_POPULAR = '[Silo] GET_MOST_POPULAR',
  GET_MOST_POPULAR_SUCCESS = '[Silo] GET_MOST_POPULAR_SUCCESS',
  GET_MOST_POPULAR_ERROR = '[Silo] GET_MOST_POPULAR_ERROR',

  GET_MOST_POPULAR_RES = '[Silo] GET_MOST_POPULAR_RES',
  GET_MOST_POPULAR_RES_SUCCESS = '[Silo] GET_MOST_POPULAR_RES_SUCCESS',
  GET_MOST_POPULAR_RES_ERROR = '[Silo] GET_MOST_POPULAR_RES_ERROR',

  GET_MOST_POPULAR_ORGANIZATIONS = '[Silo] GET_MOST_POPULAR_ORGANIZATIONS',
  GET_MOST_POPULAR_ORGANIZATIONS_SUCCESS = '[Silo] GET_MOST_POPULAR_ORGANIZATIONS_SUCCESS',
  GET_MOST_POPULAR_ORGANIZATIONS_ERROR = '[Silo] GET_MOST_POPULAR_ORGANIZATIONS_ERROR',

  GET_LAST_ADDED = '[Silo] GET_LAST_ADDED',
  GET_LAST_ADDED_SUCCESS = '[Silo] GET_LAST_ADDED_SUCCESS',
  GET_LAST_ADDED_ERROR = '[Silo] GET_LAST_ADDED_ERROR',

  GET_RECENTLY_UPDATED_RES = '[Silo] GET_RECENTLY_UPDATED_RES',
  GET_RECENTLY_UPDATED_RES_SUCCESS = '[Silo] GET_RECENTLY_UPDATED_RES_SUCCESS',
  GET_RECENTLY_UPDATED_RES_ERROR = '[Silo] GET_RECENTLY_UPDATED_RES_ERROR',

  GET_LAST_ADDED_RES = '[Silo] GET_LAST_ADDED_RES',
  GET_LAST_ADDED_RES_SUCCESS = '[Silo] GET_LAST_ADDED_RES_SUCCESS',
  GET_LAST_ADDED_RES_ERROR = '[Silo] GET_LAST_ADDED_RES_ERROR',

  GET_LAST_ADDED_PROJECTS = '[Silo] GET_LAST_ADDED_PROJECTS',
  GET_LAST_ADDED_PROJECTS_SUCCESS = '[Silo] GET_LAST_ADDED_PROJECTS_SUCCESS',
  GET_LAST_ADDED_PROJECTS_ERROR = '[Silo] GET_LAST_ADDED_PROJECTS_ERROR',

  GET_LAST_ADDED_ORGANIZATIONS = '[Silo] GET_LAST_ADDED_ORGANIZATIONS',
  GET_LAST_ADDED_ORGANIZATIONS_SUCCESS = '[Silo] GET_LAST_ADDED_ORGANIZATIONS_SUCCESS',
  GET_LAST_ADDED_ORGANIZATIONS_ERROR = '[Silo] GET_LAST_ADDED_ORGANIZATIONS_ERROR',

  SET_NEXT_TSFAV_PAGE = '[Silo] SET_NEXT_TSFAV_PAGE',
  SET_PREV_TSFAV_PAGE = '[Silo] SET_PREV_TSFAV_PAGE',

  SET_NEXT_LRFAV_PAGE = '[Silo] SET_NEXT_LRFAV_PAGE',
  SET_PREV_LRFAV_PAGE = '[Silo] SET_PREV_LRFAV_PAGE',

  SET_NEXT_OFAV_PAGE = '[Silo] SET_NEXT_OFAV_PAGE',
  SET_PREV_OFAV_PAGE = '[Silo] SET_PREV_OFAV_PAGE',

  SET_NEXT_TS_PAGE = '[Silo] SET_NEXT_TS_PAGE',
  SET_PREV_TS_PAGE = '[Silo] SET_PREV_TS_PAGE',

  SET_NEXT_TSMP_PAGE = '[Silo] SET_NEXT_TSMP_PAGE',
  SET_PREV_TSMP_PAGE = '[Silo] SET_PREV_TSMP_PAGE',

  SET_WIDGET_TAB = '[Silo] SET_WIDGET_TAB',

  SET_NEXT_LR_PAGE = '[Silo] SET_NEXT_LR_PAGE',
  SET_PREV_LR_PAGE = '[Silo] SET_PREV_LR_PAGE',

  SET_NEXT_LRMP_PAGE = '[Silo] SET_NEXT_LRMP_PAGE',
  SET_PREV_LRMP_PAGE = '[Silo] SET_PREV_LRMP_PAGE',

  SET_NEXT_O_PAGE = '[Silo] SET_NEXT_O_PAGE',
  SET_PREV_O_PAGE = '[Silo] SET_PREV_O_PAGE',

  SET_NEXT_OMP_PAGE = '[Silo] SET_NEXT_OMP_PAGE',
  SET_PREV_OMP_PAGE = '[Silo] SET_PREV_OMP_PAGE'
}

/* SEARCH */
export class GetSearchResults implements Action {
  readonly type = SearchResultTypes.GET_SEARCH_RESULTS;

  constructor(public payload: string) {}
}

export class GetSearchResultsSuccess implements Action {
  readonly type = SearchResultTypes.GET_SEARCH_RESULTS_SUCCESS;

  constructor(public payload: SearchResults) {}
}

export class GetSearchResultsNextPage implements Action {
  readonly type = SearchResultTypes.GET_SEARCH_RESULTS_NEXTPAGE;

  constructor(public payload: string) {}
}

export class GetSearchResultsNextPageSuccess implements Action {
  readonly type = SearchResultTypes.GET_SEARCH_RESULTS_NEXTPAGE_SUCCESS;

  constructor(public payload: SearchResults) {}
}

export class GetSearchResultsError implements Action {
  readonly type = SearchResultTypes.GET_SEARCH_RESULTS_ERROR;
}

/* SILO */

export class GetTopCategories implements Action {
  readonly type = SearchResultTypes.GET_TOP_CATEGORIES;
}

export class GetTopCategoriesSuccess implements Action {
  readonly type = SearchResultTypes.GET_TOP_CATEGORIES_SUCCESS;

  constructor(public payload: TopCategories) {}
}

export class GetTopCategoriesError implements Action {
  readonly type = SearchResultTypes.GET_TOP_CATEGORIES_ERROR;

  constructor(public payload: any) {}
}

/* RECENTLY UPDATED */

export class GetRecentlyUpdated implements Action {
  readonly type = SearchResultTypes.GET_RECENTLY_UPDATED;
}

export class GetRecentlyUpdatedSuccess implements Action {
  readonly type = SearchResultTypes.GET_RECENTLY_UPDATED_SUCCESS;

  constructor(public payload: RecentlyUpdatedTech) {}
}

export class GetRecentlyUpdatedError implements Action {
  readonly type = SearchResultTypes.GET_RECENTLY_UPDATED_ERROR;

  constructor(public payload: any) {}
}

/* MOST POPULAR */

export class GetMostPopular implements Action {
  readonly type = SearchResultTypes.GET_MOST_POPULAR;
}

export class GetMostPopularSuccess implements Action {
  readonly type = SearchResultTypes.GET_MOST_POPULAR_SUCCESS;

  constructor(public payload: MostPopularTech) {}
}

export class GetMostPopularError implements Action {
  readonly type = SearchResultTypes.GET_MOST_POPULAR_ERROR;

  constructor(public payload: any) {}
}

/* LAST ADDED */

export class GetLastAdded implements Action {
  readonly type = SearchResultTypes.GET_LAST_ADDED;
}

export class GetLastAddedSuccess implements Action {
  readonly type = SearchResultTypes.GET_LAST_ADDED_SUCCESS;

  constructor(public payload: SearchResultsCollection) {}
}

export class GetLastAddedError implements Action {
  readonly type = SearchResultTypes.GET_LAST_ADDED_ERROR;

  constructor(public payload: any) {}
}

/* FEATURED RESOURCE */

export class GetFeaturedToolsAndService implements Action {
  readonly type = SearchResultTypes.GET_FEATURED_TOOLS_AND_SERVICE;
}

export class GetFeaturedToolsAndServiceSuccess implements Action {
  readonly type = SearchResultTypes.GET_FEATURED_TOOLS_AND_SERVICE_SUCCESS;

  constructor(public payload: SearchResultsCollection) {}
}

export class GetFeaturedToolsAndServiceError implements Action {
  readonly type = SearchResultTypes.GET_FEATURED_TOOLS_AND_SERVICE_ERROR;

  constructor(public payload: any) {}
}

/* FEATURED LANGUAGES RESOURCES */

export class GetFeaturedLanguageResources implements Action {
  readonly type = SearchResultTypes.GET_FEATURED_LANGUAGE_RESOURCES;
}

export class GetFeaturedLanguageResourcesSuccess implements Action {
  readonly type = SearchResultTypes.GET_FEATURED_LANGUAGE_RESOURCES_SUCCESS;

  constructor(public payload: SearchResultsCollection) {}
}

export class GetFeaturedLanguageResourcesError implements Action {
  readonly type = SearchResultTypes.GET_FEATURED_LANGUAGE_RESOURCES_ERROR;

  constructor(public payload: any) {}
}

/* FEATURED ORGANIZATIONS */

export class GetFeaturedOrganizations implements Action {
  readonly type = SearchResultTypes.GET_FEATURED_ORGANIZATIONS;
}

export class GetFeaturedOrganizationsSuccess implements Action {
  readonly type = SearchResultTypes.GET_FEATURED_ORGANIZATIONS_SUCCESS;

  constructor(public payload: SearchResultsCollection) {}
}

export class GetFeaturedOrganizationsError implements Action {
  readonly type = SearchResultTypes.GET_FEATURED_ORGANIZATIONS_ERROR;

  constructor(public payload: any) {}
}

/* RECENTLY UPDATED RESOURCE */

export class GetRecentlyUpdatedResource implements Action {
  readonly type = SearchResultTypes.GET_RECENTLY_UPDATED_RES;
}

export class GetRecentlyUpdatedResourceSuccess implements Action {
  readonly type = SearchResultTypes.GET_RECENTLY_UPDATED_RES_SUCCESS;

  constructor(public payload: SearchResultsCollection) {}
}

export class GetRecentlyUpdatedResourceError implements Action {
  readonly type = SearchResultTypes.GET_RECENTLY_UPDATED_RES_ERROR;

  constructor(public payload: any) {}
}

/* LAST ADDED RESOURCE*/

export class GetLastAddedResource implements Action {
  readonly type = SearchResultTypes.GET_LAST_ADDED_RES;
}

export class GetLastAddedResourceSuccess implements Action {
  readonly type = SearchResultTypes.GET_LAST_ADDED_RES_SUCCESS;

  constructor(public payload: SearchResultsCollection) {}
}

export class GetLastAddedResourceError implements Action {
  readonly type = SearchResultTypes.GET_LAST_ADDED_RES_ERROR;

  constructor(public payload: any) {}
}

/* MOST POPULAR ORGANIZATIONS */

export class GetMostPopularResource implements Action {
  readonly type = SearchResultTypes.GET_MOST_POPULAR_RES;
}

export class GetMostPopularResourceSuccess implements Action {
  readonly type = SearchResultTypes.GET_MOST_POPULAR_RES_SUCCESS;

  constructor(public payload: SearchResultsCollection) {}
}

export class GetMostPopularResourceError implements Action {
  readonly type = SearchResultTypes.GET_MOST_POPULAR_RES_ERROR;

  constructor(public payload: any) {}
}

/* LAST ADDED PROJECTS */

export class GetLastAddedProjects implements Action {
  readonly type = SearchResultTypes.GET_LAST_ADDED_PROJECTS;
}

export class GetLastAddedProjectsSuccess implements Action {
  readonly type = SearchResultTypes.GET_LAST_ADDED_PROJECTS_SUCCESS;

  constructor(public payload: SearchResultsCollection) {}
}

export class GetLastAddedProjectsError implements Action {
  readonly type = SearchResultTypes.GET_LAST_ADDED_PROJECTS_ERROR;

  constructor(public payload: any) {}
}

/* LAST ADDED ORGANIZATIONS */

export class GetLastAddedOrganizations implements Action {
  readonly type = SearchResultTypes.GET_LAST_ADDED_ORGANIZATIONS;
}

export class GetLastAddedOrganizationsSuccess implements Action {
  readonly type = SearchResultTypes.GET_LAST_ADDED_ORGANIZATIONS_SUCCESS;

  constructor(public payload: SearchResultsCollection) {}
}

export class GetLastAddedOrganizationsError implements Action {
  readonly type = SearchResultTypes.GET_LAST_ADDED_ORGANIZATIONS_ERROR;

  constructor(public payload: any) {}
}

/* MOST POPULAR ORGANIZATIONS */

export class GetMostPopularOrganizations implements Action {
  readonly type = SearchResultTypes.GET_MOST_POPULAR_ORGANIZATIONS;
}

export class GetMostPopularOrganizationsSuccess implements Action {
  readonly type = SearchResultTypes.GET_MOST_POPULAR_ORGANIZATIONS_SUCCESS;

  constructor(public payload: SearchResultsCollection) {}
}

export class GetMostPopularOrganizationsError implements Action {
  readonly type = SearchResultTypes.GET_MOST_POPULAR_ORGANIZATIONS_ERROR;

  constructor(public payload: any) {}
}

/* Tools and services widget paging */

export class SetToolsServicesFavNextPage implements Action {
  readonly type = SearchResultTypes.SET_NEXT_TSFAV_PAGE;
}

export class SetToolsServicesFavPrevPage implements Action {
  readonly type = SearchResultTypes.SET_PREV_TSFAV_PAGE;
}

export class SetLanguageResourcesFavNextPage implements Action {
  readonly type = SearchResultTypes.SET_NEXT_LRFAV_PAGE;
}

export class SetLanguageResourcesFavPrevPage implements Action {
  readonly type = SearchResultTypes.SET_PREV_LRFAV_PAGE;
}

export class SetOrganizationsFavNextPage implements Action {
  readonly type = SearchResultTypes.SET_NEXT_OFAV_PAGE;
}

export class SetOrganizationsFavPrevPage implements Action {
  readonly type = SearchResultTypes.SET_PREV_OFAV_PAGE;
}

export class SetToolsServicesNextPage implements Action {
  readonly type = SearchResultTypes.SET_NEXT_TS_PAGE;
}

export class SetToolsServicesPrevPage implements Action {
  readonly type = SearchResultTypes.SET_PREV_TS_PAGE;
}

export class SetToolsServicesMPNextPage implements Action {
  readonly type = SearchResultTypes.SET_NEXT_TSMP_PAGE;
}

export class SetToolsServicesMPPrevPage implements Action {
  readonly type = SearchResultTypes.SET_PREV_TSMP_PAGE;
}

export class SetWidgetTab implements Action {
  readonly type = SearchResultTypes.SET_WIDGET_TAB;
  constructor(public widget: string, public tab: number) {}
}

/* Language resources widget paging */

export class SetLanguageResourcesNextPage implements Action {
  readonly type = SearchResultTypes.SET_NEXT_LR_PAGE;
}

export class SetLanguageResourcesPrevPage implements Action {
  readonly type = SearchResultTypes.SET_PREV_LR_PAGE;
}

export class SetLanguageResourcesMPNextPage implements Action {
  readonly type = SearchResultTypes.SET_NEXT_LRMP_PAGE;
}

export class SetLanguageResourcesMPPrevPage implements Action {
  readonly type = SearchResultTypes.SET_PREV_LRMP_PAGE;
}

/* Organizations widget paging */

export class SetOrganizationsNextPage implements Action {
  readonly type = SearchResultTypes.SET_NEXT_O_PAGE;
}

export class SetOrganizationsPrevPage implements Action {
  readonly type = SearchResultTypes.SET_PREV_O_PAGE;
}

export class SetOrganizationsMPNextPage implements Action {
  readonly type = SearchResultTypes.SET_NEXT_OMP_PAGE;
}

export class SetOrganizationsMPPrevPage implements Action {
  readonly type = SearchResultTypes.SET_PREV_OMP_PAGE;
}

export type Actions =
  | GetSearchResults
  | GetSearchResultsSuccess
  | GetSearchResultsError
  | GetSearchResultsNextPage
  | GetSearchResultsNextPageSuccess
  | GetTopCategories
  | GetTopCategoriesSuccess
  | GetTopCategoriesError
  | GetFeaturedToolsAndService
  | GetFeaturedToolsAndServiceSuccess
  | GetFeaturedToolsAndServiceError
  | GetFeaturedLanguageResources
  | GetFeaturedLanguageResourcesSuccess
  | GetFeaturedLanguageResourcesError
  | GetFeaturedOrganizations
  | GetFeaturedOrganizationsSuccess
  | GetFeaturedOrganizationsError
  | GetRecentlyUpdated
  | GetRecentlyUpdatedSuccess
  | GetRecentlyUpdatedError
  | GetMostPopular
  | GetMostPopularSuccess
  | GetMostPopularError
  | GetMostPopularResource
  | GetMostPopularResourceSuccess
  | GetMostPopularResourceError
  | GetMostPopularOrganizations
  | GetMostPopularOrganizationsSuccess
  | GetMostPopularOrganizationsError
  | GetLastAdded
  | GetLastAddedSuccess
  | GetLastAddedError
  | GetRecentlyUpdatedResource
  | GetRecentlyUpdatedResourceSuccess
  | GetRecentlyUpdatedResourceError
  | GetLastAddedResource
  | GetLastAddedResourceSuccess
  | GetLastAddedResourceError
  | GetLastAddedProjects
  | GetLastAddedProjectsSuccess
  | GetLastAddedProjectsError
  | GetLastAddedOrganizations
  | GetLastAddedOrganizationsSuccess
  | GetLastAddedOrganizationsError
  | SetToolsServicesFavNextPage
  | SetToolsServicesFavPrevPage
  | SetLanguageResourcesFavNextPage
  | SetLanguageResourcesFavPrevPage
  | SetOrganizationsFavNextPage
  | SetOrganizationsFavPrevPage
  | SetToolsServicesNextPage // Tools Services
  | SetToolsServicesPrevPage
  | SetToolsServicesMPNextPage
  | SetToolsServicesMPPrevPage
  | SetWidgetTab
  | SetLanguageResourcesNextPage // Language Resaurces
  | SetLanguageResourcesPrevPage
  | SetLanguageResourcesMPNextPage
  | SetLanguageResourcesMPPrevPage
  | SetOrganizationsNextPage // Organizations
  | SetOrganizationsPrevPage
  | SetOrganizationsMPNextPage
  | SetOrganizationsMPPrevPage;

