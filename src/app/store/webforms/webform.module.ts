import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { MetaReducer, StoreModule } from '@ngrx/store';
import { CmsMenuService } from '../../+cms/cms-menu.service';

import { WebformEffects } from './webform.effects';
import * as fromWebform from './webform.reducer';

@NgModule({
  imports: [CommonModule, StoreModule.forFeature('webforms', fromWebform.WebformReducer, {}), EffectsModule.forFeature([WebformEffects])],
  providers: [CmsMenuService]
})
export class WebformModule {}
