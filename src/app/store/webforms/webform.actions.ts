import { Action } from '@ngrx/store';

import { WebFormT } from '../../library/webforms/webforms.model';

export enum WebformActionTypes {
  GET_WEBFORM_FIELDS = '[Webform] GET_WEBFORM_FIELDS',
  GET_WEBFORM_FIELDS_SUCCESS = '[Webform] GET_WEBFORM_FIELDS_SUCCESS',
  GET_WEBFORM_FIELDS_ERROR = '[Webform] GET_WEBFORM_FIELDS_ERROR',
  GET_WEBFORM_ELEMENTS = '[Webform] GET_WEBFORM_ELEMENTS',
  GET_WEBFORM_ELEMENTS_SUCCESS = '[Webform] GET_WEBFORM_ELEMENTS_SUCCESS',
  GET_WEBFORM_ELEMENTS_ERROR = '[Webform] GET_WEBFORM_ELEMENTS_ERROR',
  POST_WEBFORM = '[Webform] POST_WEBFORM',
  POST_WEBFORM_SUCCESS = '[Webform] POST_WEBFORM_SUCCESS',
  POST_WEBFORM_ERROR = '[Webform] POST_WEBFORM_ERROR',
}

export class GetWebformFields implements Action {
  readonly type = WebformActionTypes.GET_WEBFORM_FIELDS;

  constructor(public payload: string) {}
}

export class GetWebformFieldsSuccess implements Action {
  readonly type = WebformActionTypes.GET_WEBFORM_FIELDS_SUCCESS;

  constructor(public payload: WebFormT) {}
}

export class GetWebformFieldsError implements Action {
  readonly type = WebformActionTypes.GET_WEBFORM_FIELDS_ERROR;

  constructor(public payload: string) {}
}

export class PostWebform implements Action {
  readonly type = WebformActionTypes.POST_WEBFORM;

  constructor(public payload: any) {}
}

export class PostWebformSuccess implements Action {
  readonly type = WebformActionTypes.POST_WEBFORM_SUCCESS;

  constructor(public payload: any) {}
}

export class PostWebformError implements Action {
  readonly type = WebformActionTypes.POST_WEBFORM_ERROR;

  constructor(public payload: any) {}
}

export class GetWebformElements implements Action {
  readonly type = WebformActionTypes.GET_WEBFORM_ELEMENTS;

  constructor(public payload: string) {}
}

export class GetWebformElementsSuccess implements Action {
  readonly type = WebformActionTypes.GET_WEBFORM_ELEMENTS_SUCCESS;

  constructor(public payload: WebFormT) {}
}

export class GetWebformElementsError implements Action {
  readonly type = WebformActionTypes.GET_WEBFORM_ELEMENTS_ERROR;

  constructor(public payload: string) {}
}

export type Actions =
  | GetWebformFields
  | GetWebformFieldsSuccess
  | GetWebformFieldsError
  | GetWebformElements
  | GetWebformElementsSuccess
  | GetWebformElementsError
  | PostWebform
  | PostWebformSuccess
  | PostWebformError;
