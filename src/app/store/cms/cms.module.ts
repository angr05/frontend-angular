import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { MetaReducer, StoreModule } from '@ngrx/store';
import { CmsMenuService } from '../../+cms/cms-menu.service';
import { CmsService } from '../../+cms/cms.service';

import { CmsEffects } from './cms.effects';
import * as fromCms from './cms.reducer';

@NgModule({
  imports: [CommonModule, StoreModule.forFeature('cms', fromCms.CmsReducer, {}), EffectsModule.forFeature([CmsEffects])],
  providers: [CmsMenuService, CmsService]
})
export class CmsStateModule {}
