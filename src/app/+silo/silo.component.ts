/* eslint-disable class-methods-use-this */
import { ChangeDetectorRef, Component, Inject, Injector, OnInit, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from "@angular/common";
import { BaseComponent } from '../framework/core';
import { routeAnimation } from '../shared';
import { Store } from '@ngrx/store';
import { State } from '../store/state';

import { SearchResults, TopCategories, MostPopularTech, RecentlyUpdatedTech, SearchResultsCollection } from '../library/search/models/search-results.model';
import * as SearchAction from '../store/search/search.actions';

@Component({
  selector: 'app-silo',
  templateUrl: './silo.component.html',
  styleUrls: ['./silo.component.scss'],
  animations: [routeAnimation],
})
export class SiloComponent extends BaseComponent implements OnInit {
  userDetails: any;
  isAuthenticated = false;
  isBrowser = isPlatformBrowser(this.platformId);
  searchResults: SearchResults;
  topCategories: TopCategories;
  recentlyUpdatedTech: RecentlyUpdatedTech;
  mostPopularTech: MostPopularTech;
  latestAddedTech: SearchResultsCollection;
  showAllCategories = false;
  showAllRecents = false;
  showAllPopulars = false;
  showAllLatestAdded = false;

  constructor(
    private readonly appStore: Store<State>, 
    private readonly cd: ChangeDetectorRef,
    @Inject(PLATFORM_ID) private readonly platformId: any
  ) {
    super();
   }

  async ngOnInit(): Promise<void> {
    this.appStore.dispatch(new SearchAction.GetTopCategories());
    this.appStore.dispatch(new SearchAction.GetMostPopular());
    this.appStore.dispatch(new SearchAction.GetRecentlyUpdated());
    this.appStore.dispatch(new SearchAction.GetLastAdded());

    this.appStore.select((appState: any) => appState.catalogue.topCategories
    ).subscribe((_topCategories: any) => {
     this.topCategories = _topCategories.categories;
     this.cd.markForCheck();
   });

      this.appStore.select((appState: any) => appState.catalogue.recentlyUpdatedTech
      ).subscribe((_recentlyUpdatedTech: any) => {
        if(_recentlyUpdatedTech && _recentlyUpdatedTech.results.length !== undefined){
          this.recentlyUpdatedTech = _recentlyUpdatedTech.results;
          this.cd.markForCheck();
        }

      });

      this.appStore.select((appState: any) => appState.catalogue.mostPopularTech
      ).subscribe((_mostPopularTech: any) => {
        if(_mostPopularTech && _mostPopularTech.results.length !== undefined){
        this.mostPopularTech = _mostPopularTech.results; 
        this.cd.markForCheck();
        }
      });

      this.appStore.select((appState: any) => appState.catalogue.latestAddedTech
      ).subscribe((_latestAddedTech: any) => {
        if(_latestAddedTech && _latestAddedTech.results.length !== undefined){
        this.latestAddedTech = _latestAddedTech.results; 
        this.cd.markForCheck();
        }
      });
  }

  toggleShowAllCategories($event):void {
    $event.preventDefault();
    this.showAllCategories = !this.showAllCategories;
  }

  toggleShowAllPopular($event):void {
    $event.preventDefault();
    this.showAllPopulars = !this.showAllPopulars;
  }

  toggleShowAllRecent($event):void {
    $event.preventDefault();
    this.showAllRecents = !this.showAllRecents;
  }

  toggleShowAllLatestAdded($event):void {
    $event.preventDefault();
    this.showAllLatestAdded = !this.showAllLatestAdded;
  }

}
