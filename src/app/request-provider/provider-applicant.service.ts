import { Injectable } from '@angular/core';
import { CookieService } from '../layout/foot/cookie.service';

@Injectable({
  providedIn: 'root'
})
export class ProviderApplicantService {

  constructor(    private readonly cookieService: CookieService) { }

  // eslint-disable-next-line class-methods-use-this
  userIsProvider(roles: Array<string>): boolean {
    
    return (roles && roles.length > 0 && roles.includes("provider"));
  }

  getProviderApplicants(): Array<string> {
    const prov = this.cookieService.getCookie("provider_status_requested");
    let providerApplicants: Array<string> = [];
    if (prov !== "") {
      providerApplicants = prov.split(",");
    }

    return providerApplicants;
  }

  addProviderApplicant(userId: string): void {
    let prov = this.cookieService.getCookie("provider_status_requested");
    if (prov) {
      prov += `,${userId}`;
    } else {
      prov += userId;
    }

    this.cookieService.setCookie("provider_status_requested", prov, 365);
  }

  userHasAppliedForProvider(userId: string): boolean {
    const providerApplicants = this.getProviderApplicants();

    return (providerApplicants.length > 0 && providerApplicants.includes(userId));
  }

  removeProviderApplicant(userId: string): void {
    if (this.userHasAppliedForProvider(userId)) {
      let applicants = this.getProviderApplicants();
      const userIndex = applicants.findIndex(uid => uid === userId);
      if (userIndex !== -1){
        applicants = applicants.splice(userIndex,1);
        this.cookieService.setCookie("provider_status_requested", applicants.join(","), 365);
      }
    }
  }
}
