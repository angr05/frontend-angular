import { TestBed } from '@angular/core/testing';

import { ProviderApplicantService } from './provider-applicants.service';

describe('ProviderApplicantService', () => {
  let service: ProviderApplicantService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProviderApplicantService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
