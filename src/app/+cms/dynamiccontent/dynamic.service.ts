/* eslint-disable no-null/no-null */
/* eslint-disable class-methods-use-this */

import { ElementRef, Injectable, Renderer2, RendererFactory2 } from '@angular/core';

import { DomService } from '../dom.service';
import { DynamicComponent } from './dynamic.component';
import { InteractiveData } from './interactive.model';
import { Router } from '@angular/router';

@Injectable()
export class DynamicService {
  private readonly renderer: Renderer2;

  constructor(private readonly domService: DomService, rendererFactory: RendererFactory2, private readonly router: Router) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  // Funkcija lapas sadaļu parsēšanai, ievieš dinamisko saturu
  /**
   *
   * @param sectionName
   * @param sectionParsed
   * @param cmsPageContent
   * @param elRef
   * @param appendSelector
   * @param classes
   * @param buffer
   * @returns boolean if appended to DOM or HTMLElement if buffered
   */
  parseSection(
    sectionName: string,
    sectionParsed: { [section: string]: boolean },
    cmsPageContent: Array<string>,
    elRef: ElementRef,
    appendSelector: string,
    classes: Array<string>,
    buffer = false
  ): boolean | HTMLElement {
    if (!sectionParsed[sectionName]) {
      let pHtml = this.parseHtml(sectionName, cmsPageContent); // parse data
      // console.log('pHtml: ', pHtml);
      if (pHtml === undefined) {
        return false;
      }

      for (const i of classes) {
        pHtml = this.activateHtml(pHtml, i, DynamicComponent, i);
        // console.log('activated pHtml: ', pHtml);
      }

      if (!buffer) {
        return this.appendHtml(appendSelector, pHtml, elRef);
      } else {
        // if buffer, don't append to DOM, rather return activated HTML
        return pHtml;
      }
    }

    return true;
  }

  parseHtml(sectionIdToParse: string, cmsPageContent: Array<string>): HTMLElement {
    const parser = new DOMParser();

    cmsPageContent.push('</section>');
    cmsPageContent.unshift(`<section id='${sectionIdToParse}' class='${sectionIdToParse}'>`);
    const htmlDoc = parser.parseFromString(cmsPageContent.join(''), 'text/html');
    cmsPageContent.pop();
    cmsPageContent.shift();
    // console.log('htmlDoc: ', htmlDoc);

    // return htmlDoc;

    const newMultimediaSec = htmlDoc.getElementById(sectionIdToParse); // body.innerHTML; // .getElementById(sectionIdToParse);
    // console.log('newMultimediaSec: ', newMultimediaSec);

    if (newMultimediaSec !== undefined) {
      return newMultimediaSec;
    }
  }

  activateHtml(sectionToParse: HTMLElement, className: string, componentName: any, elementType: string): HTMLElement {
    const inlineElems = sectionToParse.getElementsByClassName(className);

    if (inlineElems.length > 0) {
      const elemCount = inlineElems.length;
      while (inlineElems.length > 0) {
        const elemData: InteractiveData = {
          type: elementType,
          value: inlineElems[0].innerHTML !== undefined ? inlineElems[0].innerHTML : inlineElems[0].textContent,
          classList: inlineElems[0].classList !== undefined ? inlineElems[0].classList : undefined,
          link:
            inlineElems[0].getAttribute('data-link') !== null
              ? inlineElems[0].getAttribute('data-link')
              : inlineElems[0].getAttribute('href') !== undefined
              ? inlineElems[0].getAttribute('href')
              : undefined,
          src: inlineElems[0].getAttribute('src') !== undefined ? inlineElems[0].getAttribute('src') : undefined,
          src_compressed:
            inlineElems[0].getAttribute('src_compressed') !== null
              ? inlineElems[0].getAttribute('src_compressed')
              : inlineElems[0].getAttribute('src') !== undefined
              ? inlineElems[0].getAttribute('src')
              : undefined,
          alt: inlineElems[0].getAttribute('alt') !== undefined ? inlineElems[0].getAttribute('alt') : undefined,
          styling: (inlineElems[0] as HTMLElement).style,
          genericData: inlineElems[0].getAttribute('data-generic') !== undefined ? inlineElems[0].getAttribute('data-generic') : undefined
        };

        inlineElems[0].parentNode.replaceChild(
          this.domService.appendInteractiveElement(componentName, elementType, elemData),
          inlineElems[0]
        );
      }
    }

    return sectionToParse;
  }

  appendHtml(hostContainer: string, elemToInsert: HTMLElement, elRef: ElementRef, buffer = false): boolean {
    if (elemToInsert !== undefined) {
      // const selectedContent = elRef.nativeElement.querySelectorAll(hostContainer); // document.getElementsByClassName("content__body");
      const selectedContent = document.querySelectorAll(hostContainer);
      if (selectedContent.length > 0) {
        if (!buffer) {
          selectedContent[0].innerHTML = '';
        }

        selectedContent[0].appendChild(elemToInsert);
        // console.log('selectedContent after: ', selectedContent);

        return true;
      } else {
        return false;
      }
    }
  }

  createHTMLElement(content: string, elemType: string, elemClass: string, elemId: string, href: string = null): HTMLElement {
    const parser = new DOMParser();
    let elem = '';
    let htmlDoc;
    if (elemType === 'a') {
      htmlDoc = this.renderer.createElement('a');
      this.renderer.setAttribute(htmlDoc, 'href', href.substring(4));
      this.renderer.setAttribute(htmlDoc, 'id', elemId);
      const classes: Array<string> = elemClass.split(' ');
      for (const className of classes) {
        this.renderer.addClass(htmlDoc, className);
      }

      this.renderer.listen(htmlDoc, 'click', (evt: MouseEvent) => this.onLinkClicked(evt, htmlDoc));
      this.renderer.setAttribute(htmlDoc, 'title', content);
      const txt = this.renderer.createText(content);
      this.renderer.appendChild(htmlDoc, txt);

      return htmlDoc;
    } else {
      elem = `<${elemType} id='${elemId}' class='${elemClass}' ${
        href !== null ? `href='${href.substring(4)}'` : ''
      }>${content}</${elemType}>`;

      htmlDoc = parser.parseFromString(elem, 'text/html');

      return htmlDoc.getElementById(elemId);
    }
  }

  private onLinkClicked(evt: Event, htmlDoc: any): void {
    evt.preventDefault();
    if ((htmlDoc as HTMLAnchorElement).getAttribute('href') !== null) {
      const href = (htmlDoc as HTMLAnchorElement).getAttribute('href');
      if (href) {
        console.log('onLinkClicked 3', href);
        this.router
          .navigateByUrl(href)
          .catch(err => {
            console.log(err);
          })
          .then(() => {
            console.log('');
          })
          .catch(() => '');
      }
    }
  }

  emptyHTMLContainer(hostContainer: string): void {
    const selectedContent = document.querySelectorAll(hostContainer);
    if (selectedContent.length > 0) {
      selectedContent[0].innerHTML = '';
    }
  }
}
