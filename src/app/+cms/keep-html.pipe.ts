import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

// tslint:disable-next-line:pipe-impure
@Pipe({ name: 'keepHtml', pure: false })
export class EscapeHtmlPipe implements PipeTransform {
    constructor(private readonly sanitizer: DomSanitizer) { }

    transform(content:string):SafeHtml {
        return this.sanitizer.bypassSecurityTrustHtml(content);
    }
}