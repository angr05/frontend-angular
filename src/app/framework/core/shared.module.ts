import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule, LAYOUT_CONFIG, BREAKPOINTS, DEFAULT_BREAKPOINTS,  } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import {CustomBreakPointsProvider} from '../../layout/custom-breakpoints';
import { CustomShowHideDirective } from '../../layout/cutom-breakpoints.directive';

export const BreakPointsProvider = { 
  provide: BREAKPOINTS,
  useValue: DEFAULT_BREAKPOINTS,
  multi: true
};

export const APP_LAYOUT_CONFIG = {
  addFlexToParent: true,
  addOrientationBps: false,
  disableDefaultBps: false,
  disableVendorPrefixes: false,
  serverLoaded: false,
  useColumnBasisZero: false
};

@NgModule({
  declarations: [
    CustomShowHideDirective
  ],
  exports: [CommonModule, FormsModule, FlexLayoutModule, TranslateModule],
  providers: [
    CustomBreakPointsProvider,
    // BreakPointsProvider,
    {
      provide: LAYOUT_CONFIG,
      useValue: APP_LAYOUT_CONFIG
    }
  ]
})
export class SharedModule {}
