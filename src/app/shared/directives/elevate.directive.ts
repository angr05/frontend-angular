import { Directive, ElementRef, HostListener, Input, OnChanges, Renderer2, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[app-material-elevation]'
})
export class MaterialElevationDirective implements OnChanges {
  @Input() defaultElevation = 2;

  @Input() raisedElevation = 8;

  constructor(private readonly element: ElementRef, private readonly renderer: Renderer2) {
    this.setElevation(this.defaultElevation);
  }

  ngOnChanges(_changes: SimpleChanges): void {
    this.setElevation(this.defaultElevation);
  }

  // tslint:disable-next-line:typedef
  @HostListener('mouseenter') onMouseEnter() {
    this.setElevation(this.raisedElevation);
  }

  // tslint:disable-next-line:typedef
  @HostListener('mouseleave') onMouseLeave() {
    this.setElevation(this.defaultElevation);
  }

  setElevation(amount: number): void {
    const elevationPrefix = 'mat-elevation-z';
    // remove all elevation classes
    const classesToRemove = Array.from((this.element.nativeElement as HTMLElement).classList).filter(c => c.startsWith(elevationPrefix));
    classesToRemove.forEach(c => {
      this.renderer.removeClass(this.element.nativeElement, c);
    });

    // add the given elevation class
    const newClass = `${elevationPrefix}${amount}`;
    this.renderer.addClass(this.element.nativeElement, newClass);
  }
}
