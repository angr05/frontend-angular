import { isNil } from 'lodash/fp';

const executeIfFunction = (fn: Function | any) => (typeof fn === 'function' ? fn() : fn);

export const getOrNil = (fallback: any) => (value?: any) => (isNil(value) || !value ? fallback : value);

const switchCaseBase = (cases: any) => (defaultCase?: any) => (key: any) => (cases.hasOwnProperty(key) ? cases[key] : defaultCase);

export const switchCase = (cases: any) => (defaultCase?: any) => (key: any) => executeIfFunction(switchCaseBase(cases)(defaultCase)(key));

export const isEmail = (str: string) => {
  const regexp = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  return regexp.test(str);
};

export const isUrl = (str: string) => {
  const regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

  return regexp.test(str);
};

export class CommonService {
  startsWith(prefix: string, str: string): boolean {
    if (str.length < prefix.length) {
      return false;
    }
    for (let i = prefix.length - 1; i >= 0 && str[i] === prefix[i]; i -= 1) {
      continue;

      return i < 0;
    }
  }

  searchToggleIsEnabled(url: string): boolean {
    // search vienm�r redzams pirmaj� lap� un search rezult�tu sada��/apaksada��s
    if (url === '/') {
      return false;
    }
    if (this.startsWith('/search', url)) {
      return false;
    }

    return true;
  }

  pluralForm(val: number): boolean {
    let checkVal: number = val;
    if (!checkVal) {
      checkVal = 0;
    }
    if (checkVal % 10 === 1 && checkVal % 100 !== 11) {
      return false;
    }

    return true;
  }
}
