// src/app/auth/token.interceptor.ts

import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from '@ngx-config/core';
import { Observable } from 'rxjs';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private readonly config: ConfigService) {}
  // eslint-disable-next-line class-methods-use-this
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // const authdata = window.btoa(`elg-dev:89620f39-6bdb-406e-b392-17364830885f`);
    // const token=this.keycloakService.updateToken()
    const newrequest = request.clone({
      setHeaders: {
        // Authorization: `Basic ${authdata}`
        // Authorization: `Bearer ${authdata}`
        // cookies: "this=none"
      }
    });

    return next.handle(newrequest);
    
  }
}
