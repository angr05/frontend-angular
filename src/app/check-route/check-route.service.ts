import { Injectable, Injector } from '@angular/core';

import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { PageComponent } from '../+cms/page/page.component';

@Injectable()
export class CheckRouteService {

  previousUrl: string;
  redirectedUrl: string;

  constructor(private readonly router: Router) {
    router.events
    .pipe(filter(event => event instanceof NavigationEnd))
    .subscribe((event: NavigationEnd) => {
      // console.log('event ', event.url, event);
      if(event.urlAfterRedirects !== event.url) {

        this.redirectedUrl = this.previousUrl;
        // console.log('saving this.redirectedUrl: ', this.redirectedUrl, this.previousUrl);
      }
      this.previousUrl = event.url;

    });
  }

  private static routeExists(path: string, router: Router): boolean {

    return router.config.findIndex((item) => item.path === path ) !== -1;
   }

  addRoute(newPath: string): void {
    if (!CheckRouteService.routeExists(newPath, this.router) && newPath !== '/catalogue') {
          this.router.config.unshift(
            { path: newPath.replace(/^\/+/, '') , component: PageComponent}
          );
    }

  }



}