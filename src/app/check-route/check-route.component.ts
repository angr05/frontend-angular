import * as CmsAction from '../store/cms/cms.actions';

import { ActivatedRoute, NavigationEnd, NavigationStart, Router } from '@angular/router';
import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';

import { CheckRouteService } from './check-route.service';
import { CmsService } from '../+cms/cms.service';
import { ConfigService } from '@ngx-config/core';
import { HttpErrorResponse } from '@angular/common/http';
import { State } from '../store/state';
import { Store } from '@ngrx/store';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'tilde-fed-term-check-route',
  template: ''
})
export class CheckRouteComponent implements OnInit {
  landingPageData;
  pageContent;
  cmsServiceUrl;
  isLoading = false;
  isBrowser = isPlatformBrowser(this.platformId);

  constructor(
    public configService: ConfigService,
    private readonly cmsService: CmsService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly checkRouteService: CheckRouteService,
    private readonly cmsStore: Store<State>,
    @Inject(PLATFORM_ID) private readonly platformId: any
  ) {}

  ngOnInit(): void {
    this.isBrowser = isPlatformBrowser(this.platformId);

    const prevUrl =
      this.checkRouteService.previousUrl.substring(0, 6) === '/page/'
        ? this.checkRouteService.previousUrl.substring(6, this.checkRouteService.previousUrl.length)
        : this.checkRouteService.previousUrl;
    // console.log('prevUrl: ', prevUrl);

    /*
    if (prevUrl.substring(0,10) === '/catalogue' && this.isBrowser) {
      if(this.isBrowser){
        window.location.href = `/catalogue/`;
      }
    } */
    this.cmsService.subCmsPageContent(`${prevUrl}`).subscribe(
      (serverData: any) => {
        this.cmsStore.dispatch(new CmsAction.SaveServerData(serverData));

        this.checkRouteService.addRoute(prevUrl);
        // and redirect there
        // console.log("redirection to prevUrl",prevUrl);
        this.router.navigate([prevUrl]);
      },
      (error: HttpErrorResponse) => {
        if (error.status === 404) {
          console.log('there was an error 404');

          const redirectUrl =
            this.checkRouteService.redirectedUrl.substring(0, 6) === '/page/'
              ? this.checkRouteService.redirectedUrl.substring(6, this.checkRouteService.redirectedUrl.length)
              : this.checkRouteService.redirectedUrl;

          this.router.navigate([redirectUrl]);
        } else {
          this.router.navigate(['/']);
          console.log('there was an error', error);
        }
      }
    );
  }
}

