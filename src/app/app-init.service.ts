/* eslint-disable @typescript-eslint/promise-function-async */
import { Injectable } from '@angular/core';
import { ConfigService } from '@ngx-config/core';
import { KeycloakConfig, KeycloakService } from 'keycloak-angular';

@Injectable()
export class AppInitService {
  constructor(private readonly config: ConfigService, private readonly keycloakService: KeycloakService) {}

  async Init(): Promise<boolean> {

   return  fetch("/assets/config.local.json")
    .catch(() =>  false)
    .then((res:Response) =>  res.json()

  ).then((conf)=>{
    const keycloakConfig: KeycloakConfig = {
      url:  conf.system.keycloak_url,
      realm:  conf.system.keycloak_realm,
      clientId: conf.system.keycloak_clientId,
    };



    return this.keycloakService.init({
      config: keycloakConfig,
      initOptions: {
        onLoad: 'check-sso',
        checkLoginIframe: false
      },
      enableBearerInterceptor: true,
      bearerExcludedUrls: ['/assets', '/clients/public', '/cms', '/catalogue/api/repository/search/']
      // bearerExcludedUrls: ['']
    });

  })
    .catch(() =>  false)




  }
}
