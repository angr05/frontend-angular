/* eslint-disable class-methods-use-this */
import { Injectable } from '@angular/core';

@Injectable()
export class CookieService {


  public   getCookie(name: string): string {
    const ca: Array<string> = document.cookie.split(';');
    const caLen: number = ca.length;
    const cookieName = `${name}=`;
    let c: string;

    for (let i = 0; i < caLen; i += 1) {
      c = ca[i].replace(/^\s+/g, '');
      if (c.indexOf(cookieName) === 0) {
        return c.substring(cookieName.length, c.length);
      }
    }

    return '';
  }

  public  deleteCookie(name: string): void {
    this.setCookie(name, "", -1);
  }

  public  setCookie(name: string, value: string, expireDays: number, path = ""): void {
    const d: Date = new Date();
    d.setTime(d.getTime() + expireDays * 24 * 60 * 60 * 1000);
    const expires = `expires=${  d.toUTCString()}`;
    // const cook = `${name  }=${  value  }; domain=localhost; ${  expires  }`;
    const cook = `${name  }=${  value  }; domain=european-language-grid.eu; ${  expires  }`;
    if (path.length > 0) {
      document.cookie = `${cook}; path=${  path}`;
    }
    document.cookie = cook;
  }

}