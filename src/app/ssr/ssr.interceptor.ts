import { HttpHandler, HttpInterceptor, HttpRequest, HttpEvent } from '@angular/common/http';
import { Inject, Injectable, Optional } from '@angular/core';
import { REQUEST } from '@nguniversal/express-engine/tokens';
import { Request } from 'express';
import { Observable } from 'rxjs';

// case insensitive check against config and value
const startsWithAny = (arr: Array<string> = []) => (value = '') => arr.some(test => value.toLowerCase().startsWith(test.toLowerCase()));

// http, https, protocol relative
const isAbsoluteURL = startsWithAny(['http', '//']);

@Injectable()
export class UniversalRelativeInterceptor implements HttpInterceptor {
  constructor(@Optional() @Inject(REQUEST) protected request: Request) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.request && !isAbsoluteURL(req.url)) {
      // local json config file request. This is necessary for SSR to work

      // comment next two lines out, for SSR dev run
      const domain = 'localhost:80';
      const protocolHost = `${this.request.protocol}://${domain.split(':')[0]}`;
      /*    // uncomment this for SSR dev run
      const protocolHost = `${this.request.protocol}://${this.request.get('host')}`;
      */
      const pathSeparator = !req.url.startsWith('/') ? '/' : '';
      const url = protocolHost + pathSeparator + req.url;
      const serverRequest = req.clone({ url });

      return next.handle(serverRequest);
    } else {
      // cms request url. This is needed only for dev, which is not accessible externally.
      // Live works without these internal redirects, although these requests should make page load faster
      if (
        req.url.startsWith('https://dev.european-language-grid.eu/cms/') ||
        req.url.startsWith('https://live.european-language-grid.eu/cms/')
      ) {
        let reUrl: string = req.url;
        reUrl = reUrl.replace('https://dev.european-language-grid.eu/cms/', 'http://service-portal-frontend-cms:8090/cms/');
        reUrl = reUrl.replace('https://live.european-language-grid.eu/cms/', 'http://service-portal-frontend-cms:8090/cms/');
        const url = reUrl;
        const cmsRequest = req.clone({ url });

        return next.handle(cmsRequest);
      } else {
        // external url - do nothing

        return next.handle(req);
      }
    }
  }
}
