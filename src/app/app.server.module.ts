import { NgModule } from '@angular/core';
import { ServerModule, ServerTransferStateModule } from '@angular/platform-server';

import { AppComponentSsr } from './ssr/app.component.ssr';
import { AppModuleSsr } from './ssr/app.module.ssr';
import { FlexLayoutServerModule } from '@angular/flex-layout/server';
import { TransferStateService } from './transfer-state.service';


@NgModule({
  imports: [
    AppModuleSsr,
    ServerModule,
    FlexLayoutServerModule,
    ServerTransferStateModule
  ],
  providers: [
    TransferStateService
  ],
  bootstrap: [AppComponentSsr]
})
export class AppServerModule {}
