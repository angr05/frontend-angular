import { Interface } from "readline";

export class WebForm {
  static generateMockWebform(): WebFormT {
    return {
      ['test']: {
        '#title': '',
        '#type': '', // "textfield" - get other types and make it select
        '#required': false,
        '#default_value': '',
        '#submit__label': '',
        '#webform': '',
        '#webform_id': '',
        '#webform_key': '',
        '#webform_parent_key': '',
        '#webform_parent_flexbox': false,
        '#webform_depth': 0,
        '#webform_children': [],
        '#webform_multiple': false,
        '#webform_composite': false,
        '#webform_parents': [],
        '#admin_title': ''
      }
    };
  }

  static generateMockWebElements(): WebformElements {
    return {
      ['testElement']: {
        test_attr: '',
        '#type': 'no'
      },
      '#_webform_states': [],
      '#tree': false,
      '#parents': [],
      '#array_parents': [],
      '#weight': 0,
      '#processed': false,
      '#required': false,
      '#attributes': {
        test_attr: ''
      },
      '#title_display': '',
      '#description_display': '',
      '#errors': undefined,
      '#id': '',
      '#sorted': false
    };
  }
}

export interface WebFormT {
  [key: string]: WebformField;
}

export interface WebformField {
  '#title': string;
  '#type': string; // "textfield" - get other types and make it select
  '#required': boolean;
  '#default_value': string;
  '#submit__label': string;
  '#webform': string;
  '#webform_id': string;
  '#webform_key': string;
  '#webform_parent_key': string;
  '#webform_parent_flexbox': boolean;
  '#webform_depth': number;
  '#webform_children': Array<string>;
  '#webform_multiple': boolean;
  '#webform_composite': boolean;
  '#webform_parents': Array<string>;
  '#admin_title': string;
}

export interface WebformElements {
  [key: string]: WebformElement | any;
  '#_webform_states': Array<string>;
  '#tree': boolean;
  '#parents': Array<string>;
  '#array_parents': Array<string>;
  '#weight': number;
  '#processed': boolean;
  '#required': boolean;
  '#attributes': {
    [key: string]: string;
  };
  '#title_display': string;
  '#description_display': string;
  '#errors': undefined;
  '#id': string;
  '#sorted': boolean;
}

export interface WebformElement {
  '#title': string;
  '#type': string;
  '#required': boolean;
  '#default_value': string;
  '#webform': string;
  '#webform_id': string;
  '#webform_key': string;
  '#webform_parent_key': string;
  '#webform_parent_flexbox': boolean;
  '#webform_depth': number;
  '#webform_children': Array<string>;
  '#webform_multiple': boolean;
  '#webform_composite': boolean;
  '#webform_parents': Array<string>;
  '#admin_title': string;
  '#maxlength': number;
  '#webform_submission': undefined;
  '#access': boolean;
  '#webform_element': boolean;
  '#allowed_tags': Array<string>;
  '#element_validate': Array<string>;
  '#pre_render': Array<Array<string>>;
  '#after_build': Array<Array<string>>;
  '#_webform_access': boolean;
  '#_webform_states': Array<string>;
  '#input': boolean;
  '#size': number;
  '#autocomplete_route_name': boolean;
  '#process': Array<Array<string>>;
  '#theme': string;
  '#theme_wrappers': Array<string>;
  '#value_callback': Array<Array<string>>;
  '#defaults_loaded': boolean;
  '#tree': boolean;
  '#parents': Array<string>;
  '#array_parents': Array<string>;
  '#weight': number;
  '#processed': boolean;
  '#attributes': {
    'data-drupal-selector': string;
  };
  '#title_display': string;
  '#description_display': string;
  '#errors': undefined;
  '#id': string;
  '#name': string;
  '#value': string;
  '#ajax_processed': boolean;
  '#groups': {
    [key: string]: {
      '#group_exists': boolean;
    };
  };
  '#sorted': boolean;
  '#after_build_done': boolean;
}

export interface WebformError {
  error: boolean;
  errorMessage: any;
}
